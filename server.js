const path = require('path');
const express = require('express');
const errorHandler = require('./middleware/error');
const dotenv = require('dotenv');
//const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const connectDB = require('./config/db');
const colors = require('colors');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const mongoSanitize = require('express-mongo-sanitize');
const helment = require('helmet');
const xss = require('xss-clean');
const rateLimit = require('express-rate-limit');
const hpp = require('hpp');
const cors = require('cors');


// Load env vars
dotenv.config({ path: './config/config.env' });

// Route Files
const users = require('./routes/users');
const auth = require('./routes/auth');
const articles = require('./routes/articles');
const projects = require('./routes/projects');
const homepage = require('./routes/homepage');

connectDB();

// Initialize Express
const app = express();

// Body Parser
app.use(express.json());

// Cookie Parser
app.use(cookieParser());

// Logging Middleware
if (process.env.NODE_ENV === 'development') {
    app.use(morgan('dev'));
}

// File Upload
app.use(fileUpload());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'public')));

// Sanitize data
app.use(mongoSanitize());

// Set security headers
app.use(helment());

// Prevent cross site scripting
app.use(xss());

// Rate limiting
const limiter = rateLimit({
    windowMs: 1 * 60 * 100, // 1 mins
    max: 100
});

app.use(limiter);

// Prevent HTTP param solution
app.use(hpp());

// Enable cors
app.use(cors());

// Mount Routers
app.use('/api/v1/users', users);
app.use('/api/v1/auth', auth);
app.use('/api/v1/articles', articles);
app.use('/api/v1/projects', projects);
app.use('/api/v1/homepage', homepage);

// Error Handler
app.use(errorHandler);

const PORT = process.env.PORT || 5000;

const server = app.listen(
    PORT,
    console.log(
        `Server running in ${ process.env.NODE_ENV } node on port ${ PORT }`.yellow.bold
    )
)

// Handle unhandled promise rejections
process.on(`unhandledRejection`, (err, promise) => {
    console.log(`Error: ${ err.message }`.red.bold);
    // Close server & exit process
    server.close(() => process.exit(1));
})