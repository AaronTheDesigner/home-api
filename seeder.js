const fs = require('fs');
const mongoose = require('mongoose');
const colors = require('colors');
const dotenv = require('dotenv');

// Load Env Vars
dotenv.config({ path: './config/config.env' });

// Load Models
const User = require('./models/User');
const Article = require('./models/Article');

// Connect to database
mongoose.connect(process.env.MONGODB_URI, {
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    useUnifiedTopology: true
});

// Read JSON files
const users = JSON.parse(
    fs.readFileSync(`${ __dirname }/_data/users.json`, 'utf-8')
);

const articles = JSON.parse(
    fs.readFileSync(`${ __dirname }/_data/articles.json`, 'utf-8')
);

const importData = async () => {
    try {
        await User.create(users);
        await Article.create(articles);

        console.log(`Data imported...`.green.inverse);
        process.exit();
    } catch (err) {
        console.error(err)
    }
}

const deleteData = async () => {
    try {
        await User.deleteMany();
        await Article.deleteMany();

        console.log(`Data destroyed...`.red.inverse);
        process.exit();
    } catch (err) {
        console.error(err);
    }
}

if (process.argv[2] === '-i') {
    importData();
} else if (process.argv[2] === '-d') {
    deleteData();
}
