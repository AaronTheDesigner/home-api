const path = require('path');
const Article = require('../models/Article');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const he = require('he');
/*push
/**
 * @desc Get all Articles
 * @route GET /api/v1/articles
 * @access Public
 */
exports.getArticles = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
})

/**
 * @desc Get single Article
 * @route GET /api/v1/articles/:id
 * @access Public
 */
exports.getArticle = asyncHandler(async (req, res, next) => {
    const article = await Article.findById(req.params.id);

    const content = article.content;
    const decoded = he.decode(content);
    article.content = decoded;

    if (!article) {
        return next(
            new ErrorResponse(`Article not found with id of ${ req.params.id }`, 404)
        );
    }

    res.status(200).json({ success: true, data: article });
})

/**
 * @desc Create new Article
 * @route POST /api/v1/articles/:id
 * @access Private
 */
exports.createArticle = asyncHandler(async (req, res, next) => {
    // Add user to req body
    req.body.user = req.user.id;

    const article = await Article.create(req.body);

    res.status(201).json({
        success: true,
        data: article
    })
})


/**
 * @desc Update Article
 * @route PUT /api/v1/articles/:id
 * @access Private
 */
exports.updateArticle = asyncHandler(async (req, res, next) => {
    let article = await Article.findById(req.params.id);

    // if there is no article
    if (!article) {
        return next(
            new ErrorResponse(`Article not found with id of ${ req.params.id }`, 404)
        );
    }

    // if original poster doesn't match editor & if user role is not publisher or admin
    if (article.user.toString() !== req.user.id && req.user.role !== 'publisher' || req.user.role !== 'admin') {
        return next(
            new ErrorResponse(
                `User ${ req.user.id } is not authorized to update this article`,
                401
            )
        );
    }

    article = await Article.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: article
    })
})


/**
 * @desc Delete Article
 * @route DELETE /api/v1/articles/:id
 * @access Private
 */
exports.deleteArticle = asyncHandler(async (req, res, next) => {
    const article = await Article.findById(req.params.id);

    if (!article) {
        return next(
            new ErrorResponse(`Article not found with id of ${ req.params.id }`, 404)
        )
    }

    // Make sure user is Article owner || make sure user role is "admin"
    if (article.user.toString() !== req.user.id || req.user.role !== 'admin') {
        return next(
            new ErrorResponse(
                `User ${ req.user.id } is not authorized to delete this article`,
                401
            )
        );
    }

    article.remove();

    res.status(200).json({
        success: true,
        data: article
    })
})

/**
 * @desc Upload photo for Article
 * @route PUT /api/v1/articles/:id/photo
 * @access Private
 */
exports.articlePhotoUpload = asyncHandler(async (req, res, next) => {
    const article = await Article.findById(req.params.id);

    if (!article) {
        return next(
            new ErrorResponse(`Article not found with id of ${ req.params.id }`, 404)
        );
    }

    //Make sure user is article owner || make sure user is admin
    if (article.user.toString() !== req.user.id || req.user.role !== 'admin') {
        return next(
            new ErrorResponse(
                `User ${ req.user.id } is not authorized to update this article`,
            )
        )
    }

    if (!req.files) {
        return next(new ErrorResponse(`Please upload a file`, 400))
    }

    const file = req.files.file;

    // Confirm the image is a photo
    if (!file.mimetype.startsWith('image')) {
        return next(new ErrorResponse(`Please upload an image file`, 400))
    }

    // Check File Size
    if (file.size > process.env.MAX_FILE_UPLOAD) {
        return next(
            new ErrorResponse(`Please upload an image less than ${ MAX_FILE_UPLOAD }`, 400)
        )
    }

    // Create custom filename
    file.name = `photo_${ article._id }${ path.parse(file.name).ext }`;

    file.mv(`${ process.env.FILE_UPLOAD_PATH }/${ file.name }`, async err => {
        if (err) {
            console.log(err);
            new ErrorResponse(`Problem with file upload`, 500);
        }

        await Article.findByIdAndUpdate(req.params.id, { photo: file.name });

        res.status(200).json({
            success: true,
            data: file.name
        });

    });

});