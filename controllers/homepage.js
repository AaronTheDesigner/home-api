const path = require('path');
const Homepage = require('../models/Homepage');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');

/**
 * @desc Get all Homepage
 * @route GET /api/v1/homepage
 * @access Public
 */
exports.getHomepages = asyncHandler(async (req, res, next) => {
    res.status(200).json(res.advancedResults);
})

/**
 * @desc Get single Homepage
 * @route GET /api/v1/homepage/:id
 * @access Public
 */
exports.getHomepage = asyncHandler(async (req, res, next) => {
    const homepage = await Homepage.findById(req.params.id);

    if (!homepage) {
        return next(
            new ErrorResponse(`Homepage not found with id of ${ req.params.id }`, 404)
        );
    }

    res.status(200).json({ success: true, data: homepage });
})

/**
 * @desc Create new Homepage
 * @route POST /api/v1/homepage/:id
 * @access Private
 */
exports.createHomepage = asyncHandler(async (req, res, next) => {
    // Add user to req body
    req.body.user = req.user.id;

    const homepage = await Homepage.create(req.body);

    res.status(201).json({
        success: true,
        data: homepage
    })
})

/**
 * @desc Update Homepage
 * @route PUT /api/v1/articles/:id
 * @access Private
 */
exports.updateHomepage = asyncHandler(async (req, res, next) => {
    let homepage = await Homepage.findById(req.params.id);

    // if there is no article
    if (!homepage) {
        return next(
            new ErrorResponse(`Homepage not found with id of ${ req.params.id }`, 404)
        );
    }

    homepage = await Homepage.findByIdAndUpdate(req.params.id, req.body, {
        new: true,
        runValidators: true
    });

    res.status(200).json({
        success: true,
        data: homepage
    })
})

/**
 * @desc Delete Article
 * @route DELETE /api/v1/articles/:id
 * @access Private
 */
exports.deleteHomepage = asyncHandler(async (req, res, next) => {
    const homepage = await Homepage.findById(req.params.id);

    if (!homepage) {
        return next(
            new ErrorResponse(`Homepage not found with id of ${ req.params.id }`, 404)
        )
    }

    homepage.remove();

    res.status(200).json({
        success: true,
        data: homepage
    })
})

