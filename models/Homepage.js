const mongoose = require('mongoose');

const HomepageSchema = new mongoose.Schema(
    {
        header: {
            type: String,
            required: [true, 'Header is required'],
            trim: true,
            maxlength: [500, 'Header cannot be more than 500 characters']
        },
        about: {
            type: String,
            required: [true, 'About is required'],
            trim: true,
            maxlength: [500, 'About cannot be more than 500 characters']
        },
        mid: {
            type: String,
            trim: true,
            maxlength: [500, 'Mid cannot be more than 500 characters']
        },
        low: {
            type: String,
            trim: true,
            maxlength: [500, 'Low cannot be more than 500 characters']
        },
        contact: {
            type: String,
            required: [true, 'Contact is required'],
            trim: true,
            maxlength: [500, 'Contact cannot be more than 500 characters']
        },
        footer: {
            type: String,
            required: [true, 'Footer is required'],
            trim: true,
            maxlength: [500, 'Footer cannot be more than 500 characters']
        },
        
    },
    {
        toJSON: {virtuals: true},
        toObject: { virtuals: true }
    }
)

module.exports = mongoose.model('Homepage', HomepageSchema);