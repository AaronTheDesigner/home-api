const mongoose = rewuire('mongoose');

const ServiceSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'Please add a name for your service.'],
            unique: [true, 'Cannot add duplicate services'],
            trim: true,
            maxlength: [40, 'Name cannot be more than forty characters.']
        },
        description: {
            type: String,
            required: [true, 'Please add a description of your service.'],
            trim: true,
            maxlength: [120, 'Description cannot be more than one hundred twenty characters.']
        },
        image: {
            type: String,
            default: 'NoPhoto.jpg'
        }
    },
    {
        toJSON: {virtuals: true},
        toObject: {virtuals: true}
    }
);

module.exports = mongoose.model('Service', ServiceSchema);