const mongoose = require('mongoose');


const ProjectSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, 'Please add a title.'],
            unique: [true, 'Cannot duplicate project names.'],
            trim: true,
            maxlength: [40, 'Title cannot be more than forty characters.']
        },
        intro: {
            type: String,
            required: [true, 'Please add a short introduction of your project.'],
            trim: true,
            maxlength: [120, 'Description cannot be more than one hundred twenty characters.']
        },
        status: {
            type: [String],
            enum: [
                'personal',
                'professional'
            ]
        },
        description: {
            type: String,
            required: [true, 'Must add a description.'],
            minlength: [20, 'Description must contain more than 20 characters'],
            maxlength: [2000, 'Description limit is 300 characters.']
        },
        image: {
            type: String,
            default: 'NoPhoto.jpg'
        },
        languages: {
            type: [String],
            enum: [
                'Javascript',
                'Typescript',
                'CSS',
                'SCSS',
                'HTML5',
                'Python',
                'C#',
                'C++',
                'Ruby'
            ]
        },
        technologies: {
            type: [String],
            enum: [
                'ReactJS',
                'NodeJS',
                'Axios',
                'Redux'
            ]
        },
        client: {
            type: String
        },
        link: {
            type: String,
            match: [/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi, 'Please add a valid web address.']
        }
    },
    {
        toJSON: {virtuals: true},
        toObject: {virtuals: true}
    }
);

module.exports = mongoose.model('Project', ProjectSchema);

