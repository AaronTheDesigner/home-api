const mongoose = require('mongoose');
const slugify = require('slugify');

const ArticleSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: [true, 'Please add a title.'],
            unique: [true, 'Cannot add duplicate titles.'],
            trim: true,
            maxlength: [40, 'Title cannot be more than forty characters.']
        },
        slug: String,
        subtitle: {
            type: String,
            maxlength: [40, 'Title cannot be more than forty characters.']
        },
        description: {
            type: String,
            required: [true, 'Please add a description'],
            maxlength: [130, 'Description cannot be more than 130 characters.']
        },
        tag: {
            type: [String],
            //required: true,
            enum: [
                'Web Development',
                'Mobile Development',
                "Tutorial",
                "Diary",
                'Web Design',
                'Progressive Web Applications',
                'UI/UX',
                'Business',
                'Client Relations',
                'Work/Life Balance',
                'Play',
                'Works',
                'Other'
            ]
        },
        content: {
            type: String,
            required: true,
            minlength: [20, 'Content must contain more than 20 characters']
        },
        photo: {
            type: String,
            default: 'NoPhoto.jpg'
        },
        pub: {
            type: Boolean,
            default: false
        },
        createdAt: {
            type: Date,
            default: Date.now
        },
        user: {
            type: mongoose.Schema.ObjectId,
            ref: 'User',
            required: true
        }
    },
    {
        toJSON: { virtuals: true },
        toObject: { virtuals: true }
    }
);

// Create Article slug from the title
ArticleSchema.pre('save', function (next) {
    this.slug = slugify(this.title, { lower: true });
    next();
});

module.exports = mongoose.model('Article', ArticleSchema);