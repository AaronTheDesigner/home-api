const mongoose = require('mongoose');

const ProfileSchema = new mongoose.Schema(
    {
        firstname: {
            type: String,
            required: [true, 'Please add a first name'],
            trim: true
        },
        lastname: {
            type: String,
            required: [true, 'Please add a last name.'],
            trim: true
        },
        profession: {
            type: String,
            trim: true
        },
        company: {
            type: String,
            trim: true
        },
        about: {
            type: String,
            trim: true
        },
        image: {
            type: String,
            default: 'NoPhoto.jpg'
        },
        user: {
            type: mongoose.Schema.ObjectId,
            ref: 'User',
            require: true
        }
    }
);

module.exports = mongoose.model('Profile', ProfileSchema);

