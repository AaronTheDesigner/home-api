const mongoose = require('mongoose');

const TestimonialSchema = new mongoose.Schema(
    {
        client: {
            type: String,
            required: [true, 'Please add a client name.'],
            trim: true,
        },
        position: {
            type: String,
            trim: true
        },
        company: {
            type: String,
            trim: true
        },
        image: {
            type: String,
            default: 'NoPhoto.jpg'
        },
        statement: {
            type: String,
            required: [true, 'Please add client statement.']
        },
        link: {
            type: String,
            match: [/[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&//=]*)?/gi, 'Please add a valid web address.']
        }
    },
    {
        toJSON: {virtuals: true },
        toObject: { virtuals: true }
    }
);

module.exports = mongoose.model('Testimonial', TestimonialSchema);