const express = require('express');
const {
    getProjects,
    getProject,
    createProject,
    updateProject,
    deleteProject,
    projectPhotoUpload
} = require('../controllers/projects');

const advancedResults = require('../middleware/advancedResults');
const Project = require('../models/Project');

// Include other resource routers
// something else

const router = express.Router();

const { protect, authorize } = require('../middleware/auth');

router
    .route('/:id/photo')
    .put(protect, authorize('admin'), projectPhotoUpload)

router
    .route('/')
    .get(advancedResults(Project), getProjects)
    .post(protect, authorize('admin'), createProject);

router
    .route('/:id')
    .get(getProject)
    .put(protect, authorize('admin'), updateProject)
    .delete(protect, authorize('admin'), deleteProject)

module.exports = router;