const express = require('express');
const {
    getHomepage,
    getHomepages,
    createHomepage,
    updateHomepage,
    deleteHomepage
} = require('../controllers/homepage');

const advancedResults = require('../middleware/advancedResults');
const Homepage = require('../models/Homepage');

const router = express.Router();

const { protect, authorize } = require('../middleware/auth');

router
    .route('/')
    .get(advancedResults(Homepage), getHomepages)
    .post(protect, authorize('admin'), createHomepage);

router
    .route('/:id')
    .get(getHomepage)
    .put(protect, authorize('admin'), updateHomepage)
    .delete(protect, authorize('admin'), deleteHomepage)

module.exports = router;
