const express = require('express');
const {
    getArticles,
    getArticle,
    createArticle,
    updateArticle,
    deleteArticle,
    articlePhotoUpload
} = require('../controllers/articles');

const advancedResults = require('../middleware/advancedResults');
const Article = require('../models/Article');

// Include other resource routers
// something else

const router = express.Router();

const { protect, authorize } = require('../middleware/auth');

router
    .route('/:id/photo')
    .put(protect, authorize('contributor', 'admin'), articlePhotoUpload)


router
    .route('/')
    .get(advancedResults(Article), getArticles)
    .post(protect, authorize('contributor', 'admin'), createArticle);

router
    .route('/:id')
    .get(getArticle)
    .put(protect, authorize('contributor', 'admin'), updateArticle)
    .delete(protect, authorize('contributor', 'admin'), deleteArticle)

module.exports = router;