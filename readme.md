# Home API


> Backend API for aarontoliver.com, which is the home website of Aaron Toliver.

## Usage
rename "config/config.env.env" to "config/config.env" and update values/settings to your own

## Install Dependancies

```
npm install

```

## Run App

```
# Run in dev mode
npm run dev

# Run in production mode
npm start
```

 - Version: 1.0.0
 - License: MIT